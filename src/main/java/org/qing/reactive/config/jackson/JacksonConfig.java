package org.qing.reactive.config.jackson;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Configuration
public class JacksonConfig {
	/**
	 * 序列换成json时,将所有的long变成string 因为js中得数字类型不能包含所有的java long值
	 * @return
	 */
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		// 设置Jackson序列化时只包含不为空的字段
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		// 设置在反序列化时忽略在JSON字符串中存在，而在Java中不存在的属性
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		//序列化将Long转String类型
		SimpleModule longModule = new SimpleModule();
		longModule.addSerializer(Long.class, ToStringSerializer.instance);
		longModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
		
		//序列化将BigInteger转String类型
		SimpleModule bigIntegerModule = new SimpleModule();
		bigIntegerModule.addSerializer(BigInteger.class, ToStringSerializer.instance);
		
		//序列化将BigDecimal转String类型
		SimpleModule bigDecimalModule = new SimpleModule();
		bigDecimalModule.addSerializer(BigDecimal.class, ToStringSerializer.instance); 
		
		objectMapper.registerModule(longModule);
		objectMapper.registerModule(bigIntegerModule);
		objectMapper.registerModule(bigDecimalModule);
		return objectMapper;
	}
}
