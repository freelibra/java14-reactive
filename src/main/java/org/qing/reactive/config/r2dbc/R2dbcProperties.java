package org.qing.reactive.config.r2dbc;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("spring.r2dbc")
public class R2dbcProperties {
	
	/**
	 * 这个驱动程序需要在R2DBC中按名称查找
	 */
    private String driver;
    
    /**
     * MySQL数据库服务器的主机
     */
    private String host;
    
    /**
     * MySQL数据库服务器的端口
     */
    private Integer port;
    
    /**
     * 连接到MySQL数据库的用户
     */
    private String username;
    
    /**
     * MySQL数据库用户的密码
     */
    private String password;
    
    /**
     * MySQL连接使用的数据库
     */
    private String database;
    
    /**
     * TCP连接超时
     */
    private Long connectTimeout;
    
    private Boolean useServerPrepareStatement;
    
    /**
     * 选项指示“零日期”处理
     */
    private String zeroDateOption;
    
    /**
     * SSL启用与否
     */
    private Boolean ssl;
    
    /**
     * SSL模式
     */
    private String sslMode;
    
    private String sslCa;
    
    private String sslCert;
    
    private String sslKey;
    
    private String sslKeyPassword;
    
    private String tlsVersion;
    
    /**
     * Unix域套接字的.sock文件
     */
    private String unixSocket;
    
}
