package org.qing.reactive.dao;

import org.qing.reactive.entity.User;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;

@Repository
public interface UserDao extends ReactiveCrudRepository<User, Long> {

	@Query("SELECT * FROM tb_user WHERE name = ?")
	Flux<User> findByName(String name);

}