package org.qing.reactive.service;

import org.qing.reactive.entity.User;

import reactor.core.publisher.Flux;

public interface UserService {

	Flux<User> findByName(String name);

	Flux<User> findAll();

}
