package org.qing.reactive.service.impl;

import org.qing.reactive.dao.UserDao;
import org.qing.reactive.entity.User;
import org.qing.reactive.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Override
	public Flux<User> findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	@CachePut(value = "cacheKey", key = "'test'")
	public Flux<User> findAll() {
		return dao.findAll();
	}
}
