package org.qing.reactive.entity;

import org.springframework.data.relational.core.mapping.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Table("tb_user")
@Accessors(chain = true)
public class User extends Entity{
	
	/**
	 * 名称
	 */
	private String name;
	
}
