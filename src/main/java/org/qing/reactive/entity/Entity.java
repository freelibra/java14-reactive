package org.qing.reactive.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * 公共Entity
 * @author qing
 *
 */
@Getter
@Setter
public class Entity {
	
	/**
	 * 主键
	 */
	@Id
	private Long id;
	
	/**
	 * 备注
	 */
	private String remarks;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 变更时间
	 */
	private Date updateTime;
	
	/**
	 * 删除状态
	 */
	private Boolean deleteFlg;
	
}
