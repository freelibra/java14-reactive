/*
 Navicat Premium Data Transfer

 Source Server         : mysql_localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : cloud

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 08/04/2020 09:56:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` bigint(0) NOT NULL COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '变更时间',
  `delete_flg` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '1', '1', '2020-04-07 16:28:13', '2020-04-07 16:28:13', b'0');
INSERT INTO `tb_user` VALUES (2, '2', '2', '2020-04-07 16:28:20', '2020-04-07 16:28:20', b'0');
INSERT INTO `tb_user` VALUES (3, '3', '3', '2020-04-07 16:28:27', '2020-04-07 16:28:27', b'0');
INSERT INTO `tb_user` VALUES (4, '4', '4', '2020-04-07 16:28:32', '2020-04-07 16:28:32', b'0');
INSERT INTO `tb_user` VALUES (5, '5', '5', '2020-04-07 16:28:38', '2020-04-07 16:28:38', b'0');
INSERT INTO `tb_user` VALUES (6, '6', '6', '2020-04-07 16:28:43', '2020-04-07 16:28:43', b'0');
INSERT INTO `tb_user` VALUES (7, '7', '7', '2020-04-07 16:28:49', '2020-04-07 16:28:49', b'0');

SET FOREIGN_KEY_CHECKS = 1;
